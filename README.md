Generic NAT Gateway TerraForm Module
====================================

# Use

To create a NAT Gateway with this module you need to insert the following peace of code on your own modules:

```
// Call the module
module "nat_gateway" {
  source = "git@bitbucket.org:credibilit/terraform-nat-gateway-blueprint.git?ref=<VERSION>"

  ... <parameters> ...
}
```

Where `<VERSION>` is the desired version of *this* module. The master branch store the list of versions which can be used. The possible parameters are listed in advance on this document.

## Input Parameters

The following parameters are used on this module:

* public_subnet_ids: List of Subnet IDs of the subnets in which to place the gateways

* public_subnet_count: Count of Subnet IDs of List

* private_route_tables: List of private Route Tables to edit. Optional

* private_route_tables_count: Count of Private Subnet ID. Optional


## Output parameters

This are the outputs exposed by this module.

* id: List of IDs of the NAT Gateways

* allocation_id: List of allocation IDs of the Elastic IPs address for the gateways

* subnet_id: List of subnets ID of the subnet in which the NAT gateway is placed

* network_interface_id: List of ENI IDs of the network interface created by the NAT gateway

* private_ip: List of private IPs address of the NAT Gateways

* public_ip: List of public IPs address of the NAT Gateways