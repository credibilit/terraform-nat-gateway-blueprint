resource "aws_eip" "gw" {
  vpc = true
  count = "${var.public_subnet_count}"
}
