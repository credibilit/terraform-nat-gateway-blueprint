variable "public_subnet_ids" {
  type = "list"
  description = "List of Subnet IDs of the subnets in which to place the gateways"
}

variable "public_subnet_count" {
  type = "string"
  description = "Count of Subnet IDs of List"
}

variable "private_route_tables" {
  type = "list"
  description = "List of private Route Tables to edit"
  default = []
}

variable "private_route_tables_count" {
  type = "string"
  description = "Count of Private Subnet ID "
  default = 0
}
