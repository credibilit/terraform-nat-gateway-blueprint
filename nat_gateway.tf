resource "aws_nat_gateway" "gw" {
  allocation_id = "${element(aws_eip.gw.*.id, count.index)}"
  subnet_id     = "${element(var.public_subnet_ids, count.index)}"
  count         = "${var.public_subnet_count}"
}
