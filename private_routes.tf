resource "aws_route" "r" {
  route_table_id         = "${element(var.private_route_tables,count.index)}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${element(aws_nat_gateway.gw.*.id, count.index)}"
  count                  = "${var.private_route_tables_count}"
}
