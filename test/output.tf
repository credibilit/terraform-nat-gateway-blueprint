output "id" {
  value = "${module.nat_gw.id}"
}

output "allocation_id" {
  value = "${module.nat_gw.allocation_id}"
}

output "subnet_id" {
  value = "${module.nat_gw.subnet_id}"
}

output "network_interface_id" {
  value = "${module.nat_gw.network_interface_id}"
}

output "private_ip" {
  value = "${module.nat_gw.private_ip}"
}

output "public_ip" {
  value = "${module.nat_gw.public_ip}"
}
