module "vpc" {
  source = "git::ssh://git@bitbucket.org/credibilit/terraform-vpc-blueprint.git?ref=0.0.2"

  account = "${var.account}"
  name = "${var.name}"
  domain_name = "${var.name}.local"
  cidr_block = "${var.vpc_cidr}"
  azs = "${data.aws_availability_zones.azs.names}"
  az_count = "${var.az_count}"
  hosted_zone_comment = "An internal hosted zone for testing"
  public_subnets_cidr_block = [
    "10.0.0.0/24",
    "10.0.1.0/24"
  ]
}

# resource "aws_subnet" "private" {
#   vpc_id            = "${module.vpc.vpc}"
#   availability_zone = "${element(data.aws_availability_zones.azs.names, count.index)}"
#   cidr_block        = "${cidrsubnet(data.aws_vpc.vpc.cidr_block, 8, 10 + count.index)}"
#   count             = "${var.az_count}"
# }

module "private_subnets" {
  source = "git::ssh://git@bitbucket.org/credibilit/terraform-subnets-blueprint.git?ref=1.0.1"

  account = "${var.account}"
  vpc = "${module.vpc.vpc}"
  cidr_blocks = [
    "10.0.10.0/24",
    "10.0.11.0/24"
  ]
  azs = "${data.aws_availability_zones.azs.names}"
  prefix = "prv-${var.name}"
  route_tables = [ "${module.vpc.private_route_tables}" ]
  subnet_count = "${var.az_count}"
}
