variable account {
  default = "495770326048"
}

variable "name" {
  default = "nat-gw-acme-test"
}

data "aws_availability_zones" "azs" { }

variable "vpc_cidr" {
  default = "10.0.0.0/16"
}

variable "az_count" {
  default = 2
}
