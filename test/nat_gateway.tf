module "nat_gw" {
  source                     = "../"
  public_subnet_ids          = "${module.vpc.public_subnets}"
  public_subnet_count        = "${var.az_count}"
  private_route_tables       = "${module.vpc.private_route_tables}"
  private_route_tables_count = "${var.az_count}"
}
